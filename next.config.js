module.exports = {
    async rewrites() {
        return [
          {
            source: '/:path*',
            destination: 'https://www.metaweather.com/:path*',
          },
        ]
    },
};