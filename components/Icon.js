import React from 'react';

const Icon = (props) => {
    return (
        <>
            {props.img === 'sn' &&
                <img src="/img/weather/Snow.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'sl' &&
                <img src="/img/weather/Sleet.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'h' &&
                <img src="/img/weather/Hail.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 't' &&
                <img src="/img/weather/Thunderstorm.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'hr' &&
                <img src="/img/weather/HeavyRain.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'lr' &&
                <img src="/img/weather/LightRain.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 's' &&
                <img src="/img/weather/Shower.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'hc' &&
                <img src="/img/weather/HeavyCloud.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'lc' &&
                <img src="/img/weather/LightCloud.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
            {props.img === 'c' &&
                <img src="/img/weather/Clear.png" className={props.size === "normal" ? "" : "w-28 h-auto mr-2"} />
            }
        </>
    );
}

export default Icon;