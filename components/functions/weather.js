import axios from 'axios';
const URL = 'https://bride-weather.herokuapp.com'

export const getWeather = (location) =>{
    return axios
        .get( URL + "/api/" + location)
        .then(res =>{
            if('message' in res.data){
                return false
            }else{
                return res.data
            }
        })
        .catch(err =>{
            return err
        })
}


export const getWoid = (location) =>{
    return axios
        .get( URL + "/api/loca/" + location)
        .then(res =>{
            if('message' in res.data){
                return false
            }else{
                return res.data
            }
        })
        .catch(err =>{
            return err
        })
}