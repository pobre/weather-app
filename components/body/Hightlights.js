import React, { useEffect } from 'react';
import useState from 'react-usestateref'

const Hightlights = (props) => {

    var [data, setData, dataRef] = useState('')


    useEffect(() =>{
        if(props.json){
            setData(props.json)
        }
    },[props])



    return (
        <div className="mt-12 md:pb-12 2xl:pb-0">
            <h1 className="text-white text-2xl font-bold">Today's Hightlights</h1>
            {dataRef.current &&
                <div className="mt-5">

                    <div className="block md:flex md:space-x-14">

                        <div className="bg-gray-800 pt-8 pb-8 pr-4 pl-4 md:w-1/2">
                            <div className="text-center">
                                <h3 className="text-xl">Wind status</h3>
                            </div>
                            <div className="flex justify-center items-center relative mt-4">
                                <span className="text-8xl font-bold">{Number(dataRef.current.consolidated_weather[0].wind_speed).toFixed(0)}</span>
                                <span className="text-4xl ml-2">mph</span>
                            </div>
                            <div className="flex justify-center items-center relative mt-6">
                                <div className="w-8 h-8 bg-gray-400 rounded-full flex justify-center items-center">
                                    <svg class="w-4 h-4 transform -rotate-45" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"></path></svg>
                                </div>
                                <div className="ml-3">
                                    <span>WSW</span>
                                </div>
                            </div>
                        </div>


                        <div className="mt-10 md:mt-0 bg-gray-800 pt-8 pb-8 pr-4 pl-4 md:w-1/2">
                            <div className="text-center">
                                <h3 className="text-xl">Humidity</h3>
                            </div>
                            <div className="flex justify-center items-baseline relative mt-4">
                                <span className="text-8xl font-bold">{Number(dataRef.current.consolidated_weather[0].humidity).toFixed(0)}</span>
                                <span className="text-5xl ml-2 font-thin">%</span>
                            </div>
                            <div className="flex justify-center items-center relative mt-10 w-full">
                                <div className="flex justify-center items-center w-2/3 relative">
                                    <small className="absolute -top-5 left-1 text-gray-300 font-medium">0</small>
                                    <small className="absolute -top-5 text-gray-300 font-medium">50</small>
                                    <small className="absolute -top-5 right-1 text-gray-300 font-medium">100</small>
                                    <progress value={Number(dataRef.current.consolidated_weather[0].humidity).toFixed(0)} max="100"></progress>
                                    <small className="absolute -bottom-5 right-1 text-gray-300 font-medium">%</small>
                                </div>
                            </div>
                        </div>


                    </div>


                    <div className="block md:flex md:space-x-14 mt-8">

                        <div className="mt-10 md:mt-0 bg-gray-800 pt-8 pb-8 pr-4 pl-4 md:w-1/2">
                            <div className="text-center">
                                <h3 className="text-xl">Visibility</h3>
                            </div>
                            <div className="flex justify-center items-center relative mt-4">
                                <span className="text-8xl font-bold">{Number(dataRef.current.consolidated_weather[0].visibility).toFixed(0)}</span>
                                <span className="text-4xl ml-2">miles</span>
                            </div>
                        </div>

                        <div className="mt-10 md:mt-0 bg-gray-800 pt-8 pb-8 pr-4 pl-4 md:w-1/2">
                            <div className="text-center">
                                <h3 className="text-xl">Air Pressure</h3>
                            </div>
                            <div className="flex justify-center items-center relative mt-4">
                                <span className="text-8xl font-bold">{Number(dataRef.current.consolidated_weather[0].air_pressure).toFixed(0)}</span>
                                <span className="text-4xl ml-2">mb</span>
                            </div>
                        </div>

                    </div>



                </div>
            }
        </div>
    );
}

export default Hightlights;