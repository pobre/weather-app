import React, { useEffect } from 'react';
import useState from 'react-usestateref'
import Icon from '../Icon'

var options = { weekday: 'short', month: 'long', day: 'numeric' }

const Days = (props) => {

    var [data, setData, dataRef] = useState('')

    useEffect(() =>{
        if(props.json){
            setData(props.json)
        }
    },[props])

    var [degrees, setDegrees, degreesRef] = useState(false)

    return (
        <>
            <div className="flex justify-end space-x-4 z-10">
                <div onClick={()=>setDegrees(false)} className={"rounded-full w-9 h-9 justify-center items-center flex  font-bold cursor-pointer transition-all duration-300 " + (degreesRef.current ? 'bg-gray-500 text-white' : 'bg-white text-black')}>ºC</div>
                <div onClick={()=>setDegrees(true)} className={"rounded-full w-9 h-9 justify-center items-center flex font-bold cursor-pointer transition-all duration-300 " + (degreesRef.current ? 'bg-white text-black' : 'bg-gray-500 text-white')}>ºF</div>
            </div>
            <div className="mt-10 grid 2xl:grid-cols-5 md:grid-cols-3 grid-cols-1">
                {dataRef.current &&
                dataRef.current.consolidated_weather.map((day,i)=>{
                    return(
                        <div className={"bg-gray-800 h-56 col-span-1 pl-6 pr-6 pt-3 pb-2 relative " + (i === 0 ? ' hidden ' : '') + (i === (dataRef.current.consolidated_weather.length-1) ? ' mt-10 md:mt-10 md:mr-2 md:ml-2 2xl:mt-0 ' : ' mt-10 md:mt-10 2xl:mt-0 2xl:mr-10 md:mr-2 md:ml-2 ')}>
                            <div className="text-center text-xl font-medium">{new Date(day.applicable_date).toLocaleDateString('en-US', options) }</div>

                            <div className="justify-center flex items-center w-full">
                                <Icon img={day.weather_state_abbr} />
                            </div>

                            <div className="flex absolute bottom-3 w-full text-xl">
                                <div className="text-white w-1/2">
                                    {!degreesRef.current ? day.max_temp.toFixed(0) : (Number(day.max_temp.toFixed(0))+273)} <span>{!degreesRef.current ? 'ºC' : 'ºF'}</span>
                                </div>
                                <div className="text-gray-400 w-1/2 mr-12 justify-end flex">
                                    {!degreesRef.current ? day.min_temp.toFixed(0) : (Number(day.min_temp.toFixed(0))+273)} <span>{!degreesRef.current ? 'ºC' : 'ºF'}</span>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        </>
    );
}

export default Days;