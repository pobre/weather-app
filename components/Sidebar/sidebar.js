import React, { useEffect } from 'react';
import Icon from '../Icon'
import useState from 'react-usestateref'

var options = { weekday: 'short', month: 'long', day: 'numeric' }

const sidebar = (props) => {

    var [data, setData, dataRef] = useState('')

    useEffect(() =>{
        if(props.json){
            setData(props.json)
        }
    },[props])

    return (
        <div className="w-full h-full relative">

            {dataRef.current &&
                <>
                    {/* Buttons */}
                    <div className="flex w-full justify-center">
                        <div className="w-1/2">
                            <button onClick={()=>props.active_search()} className="bg-gray-500 text-white shadow-md pt-2 pb-2 pl-3 pr-3 cursor-pointer focus:outline-none">
                                Search for places
                            </button>
                        </div>

                        <div className="w-1/2 justify-end flex">
                            <button onClick={()=>props.active_search()} className="p-2 rounded-full bg-gray-500 shadow-md focus:outline-none focus:shadow-none">
                                <svg class="w-6 h-6 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"></path></svg>
                            </button>
                        </div>
                    </div>


                    {/* INFO */}
                    <div className="w-full mt-8">

                        <div className="justify-center flex items-center mt-28 2xl:mt-32">
                            <Icon img={dataRef.current.consolidated_weather[0].weather_state_abbr} size="normal" />
                        </div>

                        <div className="justify-center flex items-baseline mt-8 md:mt-16 2xl:mt-24 text-white">
                            <span className="text-6xl md:text-9xl font-medium ">{Number(dataRef.current.consolidated_weather[0].the_temp).toFixed(0)}</span><span className="text-xl md:text-5xl text-gray-400">ºC</span>
                        </div>
                        <div className="justify-center flex items-baseline mt-2 md:mt-8 2xl:mt-12 text-white">
                            <span className="text-2xl md:text-4xl text-gray-400 font-medium">{dataRef.current.consolidated_weather[0].weather_state_name}</span>
                        </div>
                    </div>


                    {/* FOOTER */}
                    <div className="absolute bottom-0 w-full h-24">
                        <div className="justify-center flex w-full text-gray-400 space-x-3 text-xl">
                            <div>Today</div>
                            <div>·</div>
                            <div>{new Date(dataRef.current.consolidated_weather[0].applicable_date).toLocaleDateString('en-US', options) }</div>
                        </div>
                        <div className="justify-center items-center flex w-full text-gray-400 space-x-1 text-xl mt-5">
                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                            <span>{dataRef.current.title}</span>
                        </div>
                    </div>
                </>
            }



        </div>
    );
}

export default sidebar;