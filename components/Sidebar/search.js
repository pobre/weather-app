import React, { useEffect } from 'react';
import useState from 'react-usestateref'

const search = (props) => {

    var [input, setInput, inputRef] = useState('')
    var [error, setError, errorRef] = useState(false)

    useEffect(() =>{
        setError(props.error)
    },[props])

    return (
        <div className="w-full">

            <div className="w-full justify-end flex">
                <svg onClick={()=>props.deactive_search()} class="w-8 h-8 text-white cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>
            </div>

            <div className="w-full flex mt-8 space-x-8 md:space-x-4">
                <div className={"w-3/5 lg:w-3/4 border-2 justify-start items-center flex p-3 " + (errorRef.current ? 'border-red-500' : 'border-white')}>
                    <svg class="w-6 h-6 text-gray-500" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                    <input onChange={(e)=>setInput(e.target.value)} type="text" placeholder="search location" className="focus:outline-none bg-transparent pl-2 text-white" />
                </div>
                <div className="w-1/5 lg:w-1/4 justify-end flex">
                    <button onClick={()=>{
                        if(inputRef.current){
                            props.get_data(inputRef.current, true)
                        }
                    }} className="pt-3 pb-3 pl-5 pr-5 bg-blue-700 border-2 border-blue-700 focus:outline-none">
                        <span className="font-medium text-white">Search</span>
                    </button>
                </div>
            </div>


            <div className="w-full mt-14">
                <div onClick={()=>props.get_data('44418')} className="w-full flex pr-3 items-center pl-3 pt-5 pb-5 border-2 border-transparent hover:border-gray-500 text-white text-xl cursor-pointer">
                    <span>London</span>
                    <div className="w-full justify-end flex">
                        <svg class="w-4 h-4 text-gray-500 " fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                    </div>
                </div>

                <div onClick={()=>props.get_data('766273')} className="w-full flex pr-3 items-center pl-3 pt-5 pb-5 border-2 border-transparent hover:border-gray-500 text-white text-xl cursor-pointer mt-1">
                    <span>Madrid</span>
                    <div className="w-full justify-end flex">
                        <svg class="w-4 h-4 text-gray-500 " fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                    </div>
                </div>

                <div onClick={()=>props.get_data('753692')} className="w-full flex pr-3 items-center pl-3 pt-5 pb-5 border-2 border-transparent hover:border-gray-500 text-white text-xl cursor-pointer mt-1">
                    <span>Barcelona</span>
                    <div className="w-full justify-end flex">
                        <svg class="w-4 h-4 text-gray-500 " fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                    </div>
                </div>
            </div>


        </div>
    );
}

export default search;