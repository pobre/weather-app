import React, { useEffect } from 'react';
import Head from 'next/head';
import Sidebar from './Sidebar/sidebar'
import Search from './Sidebar/search'
import $ from 'jquery'
import useState from 'react-usestateref'

const Layout = (props) => {

    var [data, setData, dataRef] = useState('')

    var [error, setError, errorRef] = useState(false)

    useEffect(() =>{
        if(props.json){
            setData(props.json)
        }

        setError(props.error)
    },[props])
    
    const toggleSearch = () =>{
        $( "#search" ).slideToggle( "slow" )
    }


    return (
        <>
            <Head>
                <title>Weather App</title>
                <meta charset="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" />
                <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" />
            </Head>
            <div className="lg:flex h-screen w-full max-h-screen bg-gray-900">

                <div className="lg:h-screen lg:relative lg:w-1/4">
                    <div className="lg:w-1/4 lg:h-screen lg:fixed z-0">
                        <div id="sidebar" className="w-full lg:w-full bg-gray-800 h-screen p-4 lg:p-12 relative">
                            <div className="h-screen absolute left-0 top-0 w-full background-image"></div>
                            <Sidebar json={dataRef.current} active_search={toggleSearch} />
                            <div id="search" className="w-full bg-gray-800 h-screen p-4 lg:p-12 absolute hidden top-0 left-0">
                                <Search error={errorRef.current} get_data={props.get_data} json={dataRef.current} deactive_search={toggleSearch} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="w-full lg:w-3/4 bg-gray-900 text-white pt-12 pb-12 pl-12 pr-12 md:pl-24 md:pr-24 2xl:pl-44 2xl:pr-44">
                    {props.children}
                </div>
            </div>
        </>
    );
}

export default Layout;